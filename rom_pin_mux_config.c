//*****************************************************************************
// rom_pin_mux_config.c
//
// configure the device pins for different signals
//
// Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
// All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the
//    distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// This file was automatically generated on 9/26/2016 at 2:42:22 PM
// by TI PinMux version
//
//*****************************************************************************

#include "pin_mux_config.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_gpio.h"
#include "pin.h"
#include "gpio.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"

//*****************************************************************************
void PinMuxConfig(void)
{


    //
    // Set unused pins to PIN_MODE_0 with the exception of JTAG pins 16,17,19,20
    //
    MAP_PinModeSet(PIN_05, PIN_MODE_0);
    MAP_PinModeSet(PIN_06, PIN_MODE_0);
    MAP_PinModeSet(PIN_07, PIN_MODE_0);
    MAP_PinModeSet(PIN_08, PIN_MODE_0);
    MAP_PinModeSet(PIN_18, PIN_MODE_0);
    MAP_PinModeSet(PIN_50, PIN_MODE_0);
    MAP_PinModeSet(PIN_52, PIN_MODE_0);
    MAP_PinModeSet(PIN_53, PIN_MODE_0);
    MAP_PinModeSet(PIN_57, PIN_MODE_0);
    MAP_PinModeSet(PIN_58, PIN_MODE_0);
    MAP_PinModeSet(PIN_59, PIN_MODE_0);
    MAP_PinModeSet(PIN_60, PIN_MODE_0);
    MAP_PinModeSet(PIN_61, PIN_MODE_0);
    MAP_PinModeSet(PIN_62, PIN_MODE_0);
    MAP_PinModeSet(PIN_63, PIN_MODE_0);

    //
    // Enable Peripheral Clocks
    //
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA1, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA2, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_GPIOA3, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);

    //	ADDR1
    // Configure PIN_55 for GPIO Input
    //
    MAP_PinTypeGPIO(PIN_55, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA0_BASE, 0x2, GPIO_DIR_MODE_IN);
    PinConfigSet(PIN_55, PIN_STRENGTH_2MA, PIN_TYPE_STD_PU);

    //	LED_RED
    // Configure PIN_64 for GPIO Output
    //
    MAP_PinTypeGPIO(PIN_64, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA1_BASE, 0x2, GPIO_DIR_MODE_OUT);

    //	LED_ORANGE
    // Configure PIN_01 for GPIO Output
    //
    MAP_PinTypeGPIO(PIN_01, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA1_BASE, 0x4, GPIO_DIR_MODE_OUT);

    //	LED_GREEN
    // Configure PIN_02 for GPIO Output
    //
    MAP_PinTypeGPIO(PIN_02, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA1_BASE, 0x8, GPIO_DIR_MODE_OUT);

    //	ADDR2
    // Configure PIN_15 for GPIO Input
    //
    MAP_PinTypeGPIO(PIN_15, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA2_BASE, 0x40, GPIO_DIR_MODE_IN);
    PinConfigSet(PIN_15, PIN_STRENGTH_2MA, PIN_TYPE_STD_PU);

    //	ADDR3
    // Configure PIN_16 for GPIO Input
    //
    MAP_PinTypeGPIO(PIN_16, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA2_BASE, 0x80, GPIO_DIR_MODE_IN);
    PinConfigSet(PIN_16, PIN_STRENGTH_2MA, PIN_TYPE_STD_PU);

    //	ADDR4
    // Configure PIN_17 for GPIO Input
    //
    MAP_PinTypeGPIO(PIN_17, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA3_BASE, 0x1, GPIO_DIR_MODE_IN);
    PinConfigSet(PIN_17, PIN_STRENGTH_2MA, PIN_TYPE_STD_PU);

    //	COMMAND PIN
    // Configure PIN_18 for GPIO Input
    //
    MAP_PinTypeGPIO(PIN_18, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA3_BASE, 0x10, GPIO_DIR_MODE_IN);
    PinConfigSet(PIN_18, PIN_STRENGTH_2MA, PIN_TYPE_STD_PD);

    //	ADDR0
    // Configure PIN_45 for GPIO Input
    //
    MAP_PinTypeGPIO(PIN_45, PIN_MODE_0, false);
    MAP_GPIODirModeSet(GPIOA3_BASE, 0x80, GPIO_DIR_MODE_IN);
    PinConfigSet(PIN_45, PIN_STRENGTH_2MA, PIN_TYPE_STD_PU);

    //
    // Configure PIN_03 for UART0 UART0_TX
    //
    MAP_PinTypeUART(PIN_03, PIN_MODE_7);

    //
    // Configure PIN_04 for UART0 UART0_RX
    //
    MAP_PinTypeUART(PIN_04, PIN_MODE_7);
}
